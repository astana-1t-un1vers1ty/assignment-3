package writers;

import abstractions.IDataAdapter;
import abstractions.IOutputWriter;
import adapters.CsvAdapter;
import adapters.JsonAdapter;
import adapters.TxtAdapter;
import models.DataObject;
import utils.DataFormats;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class OutputWriter implements IOutputWriter {
    private final List<DataObject> dataObjects;
    private String fileName;
    private IDataAdapter dataAdapter;
    private final DataFormats format;
    private String output;

    public List<DataObject> getDataObjects() {
        return dataObjects;
    }


    public OutputWriter(List<DataObject> dataObjects, DataFormats outputFormat, String fileName){
        this.dataObjects = dataObjects;
        this.fileName = fileName;
        this.format = outputFormat;
        switch (outputFormat){
            case TXT: this.fileName+=".txt";dataAdapter=new TxtAdapter();break;
            case JSON: this.fileName+=".json";dataAdapter=new JsonAdapter();break;
            case CSV: this.fileName+=".csv";dataAdapter=new CsvAdapter();break;
        }
        this.output = createOutput();
    }

    // The method that prints the data to the screen, with optional parameters for encryption and character encoding
    public void printData(List<DataObject> dataObjects, boolean encrypt, boolean encode) {
        // Loop through each data object in the list
        for (DataObject dataObject : dataObjects) {
            // Apply the additional features to the object if enabled
            dataObject.addFeatures(encrypt, encode);
            // Print the JSON representation of the object to the screen
            System.out.println(dataObject.toString());
        }
    }

    private String createOutput() {
        StringBuilder builder = new StringBuilder();
        for(int i=0;i<dataObjects.size();i++){
            builder.append(dataAdapter.Convert(dataObjects.get(i)));
            if(i!=dataObjects.size()-1&&format== DataFormats.JSON) builder.append(",");
            builder.append("\n");
        }
        var res = builder.toString();
        if(format== DataFormats.JSON) {
            res = "[\n" + res + "]";
        }
        return res;
    }

    @Override
    public void write() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(output);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }


    @Override
    public String getOutput(){
        return output;
    }

    @Override
    public void setOutput(String output){
        this.output = output;
    }


}
