package writers;

import abstractions.IOutputWriter;
import decorators.OutputDecorator;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;


public class EncryptionOutputWriter extends OutputDecorator {
    public EncryptionOutputWriter(IOutputWriter outputWriter) {
        super(outputWriter);
    }

    private byte[] encrypt(String text, String secretKey, String initializationVector) throws Exception {
        SecretKeySpec key = new SecretKeySpec(secretKey.getBytes("UTF-8"), "AES");
        IvParameterSpec iv = new IvParameterSpec(initializationVector.getBytes("UTF-8"));
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);

        return cipher.doFinal(text.getBytes("UTF-8"));
    }

    private String decrypt(byte[] encryptedData, String secretKey, String initializationVector) throws Exception {
        SecretKeySpec key = new SecretKeySpec(secretKey.getBytes("UTF-8"), "AES");
        IvParameterSpec iv = new IvParameterSpec(initializationVector.getBytes("UTF-8"));

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, key, iv);

        byte[] decryptedBytes = cipher.doFinal(encryptedData);

        return new String(decryptedBytes, "UTF-8");
    }

    @Override
    public void write(){
        byte[] encrypted = null;
        try {
            String secretKey = "YourSecretKey123";
            String initializationVector = "YourInitVector12";
            encrypted = encrypt(super.getOutput(), secretKey, initializationVector);
            String ecnryptedString = new String(encrypted,StandardCharsets.UTF_8);
            super.setOutput(ecnryptedString);
            super.write();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
