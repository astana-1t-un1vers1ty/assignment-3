package writers;

import abstractions.IOutputWriter;
import decorators.OutputDecorator;

import java.nio.charset.StandardCharsets;

public class EncodingOutputWriter extends OutputDecorator {
    public EncodingOutputWriter(IOutputWriter outputWriter) {
        super(outputWriter);
    }

    @Override
    public void write() {
        byte[] utf8Bytes = StandardCharsets.UTF_8.encode(super.getOutput()).array();
        String encodedString = new String(utf8Bytes, StandardCharsets.UTF_8);
        System.out.println(encodedString);
        super.setOutput(encodedString);
        super.write();
    }
}
