package abstractions;

import models.DataObject;

import java.util.List;

public interface IOutputWriter {
    public void write();
    public String getOutput();
    public void setOutput(String output);
}
