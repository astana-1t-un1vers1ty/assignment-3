package abstractions;

import models.DataObject;

import java.util.List;

public interface IDataAdapter {
    List<DataObject> readData(String fileName);
    DataObject parseLine(String line);

    String Convert(DataObject dataObject);
}