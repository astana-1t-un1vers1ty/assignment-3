package utils;

public enum DataFormats {
    TXT,
    JSON,
    CSV
}
