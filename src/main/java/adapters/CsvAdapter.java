package adapters;

import abstractions.IDataAdapter;
import models.DataObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvAdapter implements IDataAdapter {
    private static final String delimiter = ";";
    @Override
    public List<DataObject> readData(String fileName) {
        // Create an empty list of DataObjects
        List<DataObject> dataObjects = new ArrayList<>();
        try {
            // Create a BufferedReader to read from the file
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            // Read the first line of the file and ignore it (header)
//            reader.readLine();
            // Read the next line of the file
            String line = reader.readLine();
            // Loop until the end of the file
            while (line != null) {
                // Parse the line and create a DataObject
                DataObject dataObject = parseLine(line);
                // Add the DataObject to the list
                dataObjects.add(dataObject);
                // Read the next line of the file
                line = reader.readLine();
            }
            // Close the reader
            reader.close();
        } catch (IOException e) {
            // Handle the exception
            e.printStackTrace(System.err);
        }
        // Return the list of DataObjects
        return dataObjects;
    }

    @Override
    public DataObject parseLine(String line) {
        // Split the line by comma character
        String[] parts = line.split(delimiter);
        // Get the name, grade, and course from the parts
        String name = parts[0];
        int grade = Integer.parseInt(parts[1]);
        String course = parts[2];
        // Create and return a DataObject with these values
        return new DataObject(name, grade, course);
    }

    @Override
    public String Convert(DataObject dataObject) {
        return String.join(delimiter, dataObject.getName(), String.valueOf(dataObject.getGrade()), dataObject.getCourse());
    }
}
