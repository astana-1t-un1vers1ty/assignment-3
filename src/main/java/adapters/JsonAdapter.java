package adapters;

import abstractions.IDataAdapter;
import models.DataObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonAdapter implements IDataAdapter {

    @Override
    public List<DataObject> readData(String fileName) {
        // Create an empty list of DataObjects
        List<DataObject> dataObjects = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            // Read the whole file as a single string
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();
            while (line != null) {
                sb.append(line);
                line = reader.readLine();
            }
            reader.close();
            // Parse the string as a JSON array
            JSONArray jsonArray = new JSONArray(sb.toString());
            // Loop through each element of the array
            for (int i = 0; i < jsonArray.length(); i++) {
                // Get the element as a JSON object
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                // Parse the object and create a DataObject
                DataObject dataObject = parseLine(jsonObject.toString());
                // Add the DataObject to the list
                dataObjects.add(dataObject);
            }
        } catch (IOException e) {
            // Handle the exception
            e.printStackTrace(System.err);
        }
        // Return the list of DataObjects
        return dataObjects;
    }

    @Override
    public DataObject parseLine(String line) {
        // Parse the line as a JSON object
        JSONObject jsonObject = new JSONObject(line);
        // Get the name, grade, and course from the object
        String name = jsonObject.getString("name");
        int grade = jsonObject.getInt("grade");
        String course = jsonObject.getString("course");
        // Create and return a DataObject with these values
        return new DataObject(name, grade, course);
    }

    @Override
    public String Convert(DataObject dataObject) {
        return new JSONObject(dataObject).toString();
    }
}
