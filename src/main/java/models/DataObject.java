package models;

import org.json.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Base64;

public class DataObject {
    // The fields for name, grade, and course
    private String name;
    private int grade;
    private String course;

    // The constructor that takes name, grade, and course as input
    public DataObject(String name, int grade, String course) {
        this.name = name;
        this.grade = grade;
        this.course = course;
    }

    public DataObject setName(String name) {
        this.name = name;
        return this;
    }

    public DataObject setGrade(int grade) {
        this.grade = grade;
        return this;
    }

    public DataObject setCourse(String course) {
        this.course = course;
        return this;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public String getCourse() {
        return course;
    }

    // The method that adds additional features to the object, such as encryption and character encoding
    public void addFeatures(boolean encrypt, boolean encode) {
        // If encryption is enabled, encrypt the name, grade, and course using AES
        if (encrypt) {
            try {
                // Create a secret key with a predefined value
                String key = "secret";
                Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
                // Create a cipher instance for AES encryption
                Cipher cipher = Cipher.getInstance("AES");
                // Initialize the cipher with the secret key in encryption mode
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
                // Encrypt the name, grade, and course using the cipher and encode them in Base64
                name = Base64.getEncoder().encodeToString(cipher.doFinal(name.getBytes()));
                grade = Integer.parseInt(Base64.getEncoder().encodeToString(cipher.doFinal(String.valueOf(grade).getBytes())));
                course = Base64.getEncoder().encodeToString(cipher.doFinal(course.getBytes()));
            } catch (Exception e) {
                // Handle the exception
                e.printStackTrace(System.err);
            }
        }
        // If character encoding is enabled, encode the name, grade, and course in UTF-8
        if (encode) {
            try {
                // Encode the name, grade, and course in UTF-8 and assign them to the fields
                name = new String(name.getBytes(), StandardCharsets.UTF_8);
                grade = Integer.parseInt(new String(String.valueOf(grade).getBytes(), StandardCharsets.UTF_8));
                course = new String(course.getBytes(), StandardCharsets.UTF_8);
            } catch (Exception e) {
                // Handle the exception
                e.printStackTrace(System.err);
            }
        }
    }

    // The method that returns a JSON representation of the object
    @Override
    public String toString() {
        // Create a JSON object with the fields as keys and values
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("grade", grade);
        jsonObject.put("course", course);
        // Return the JSON object as a string
        return jsonObject.toString();
    }
}
