import abstractions.IDataAdapter;
import adapters.CsvAdapter;
import adapters.JsonAdapter;
import adapters.TxtAdapter;
import models.DataObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataAggregator {
    // The list of DataObjects that store the aggregated data
    private List<DataObject> dataObjects;

    // The constructor that takes an array of file names as input
    public DataAggregator(String... fileNames) {
        parseData(fileNames);
    }

    private void parseData(String[] fileNames) {
        // Initialize the list of DataObjects
        dataObjects = new ArrayList<>();
        // Loop through each file name
        for (String fileName : fileNames) {
            // Create an adapter based on the file extension
            IDataAdapter adapter = createAdapter(fileName);
            // Read the data from the file using the adapter
            List<DataObject> data = adapter.readData(fileName);
            // Add the data to the list of DataObjects
            dataObjects.addAll(data);
        }
    }

    // The method that creates an adapter based on the file extension
    private IDataAdapter createAdapter(String fileName) {
        // Get the file extension by splitting the file name by dot
        String[] parts = fileName.split("\\.");
        String extension = parts[parts.length - 1];
        // Return a different adapter based on the extension
        switch (extension) {
            case "txt":
                return new TxtAdapter();
            case "csv":
                return new CsvAdapter();
            case "json":
                return new JsonAdapter();
            default:
                throw new IllegalArgumentException("Unsupported file format: " + extension);
        }
    }

    // The method that returns the final dataset as a JSON string
    public String getFinalDataset() {
        // Create a JSON array to store the data objects
        JSONArray jsonArray = new JSONArray();
        // Loop through each data object
        for (DataObject dataObject : dataObjects) {
            // Add the JSON representation of the object to the array
            jsonArray.put(new JSONObject(dataObject.toString()));
        }
        // Return the JSON array as a string
        return jsonArray.toString();
    }

    public List<DataObject> getDataObjects(){
        return this.dataObjects;
    }
}
