import models.DataObject;

import java.util.List;

public class OutputWriter {
    // The method that prints the data to the screen, with optional parameters for encryption and character encoding
    public void printData(List<DataObject> dataObjects, boolean encrypt, boolean encode) {
        // Loop through each data object in the list
        for (DataObject dataObject : dataObjects) {
            // Apply the additional features to the object if enabled
            dataObject.addFeatures(encrypt, encode);
            // Print the JSON representation of the object to the screen
            System.out.println(dataObject.toString());
        }
    }
}
