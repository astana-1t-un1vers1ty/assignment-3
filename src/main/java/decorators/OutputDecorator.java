package decorators;


import abstractions.IOutputWriter;
import models.DataObject;

import java.util.List;

public abstract class OutputDecorator implements IOutputWriter {

    private IOutputWriter outputWriter;

    public OutputDecorator(IOutputWriter outputWriter){
        this.outputWriter = outputWriter;
    }

    @Override
    public void write() {
        outputWriter.write();
    }


    @Override
    public String getOutput() {
        return outputWriter.getOutput();
    }

    @Override
    public void setOutput(String output) {
        outputWriter.setOutput(output);
    }

}
