import abstractions.IOutputWriter;
import utils.DataFormats;
import writers.EncodingOutputWriter;
import writers.EncryptionOutputWriter;
import writers.OutputWriter;

public class Main {
    public static void main(String[] args) {
        DataAggregator dataAggregator = new DataAggregator("student.csv", "student.txt", "student.json");
        IOutputWriter outputWriter = new EncodingOutputWriter(
                new EncryptionOutputWriter(
                new OutputWriter(dataAggregator.getDataObjects(), DataFormats.CSV, "output")));
        outputWriter.write();
    }
}
